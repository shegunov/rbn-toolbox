#include <iostream>
#include <cassert>

#include "rbnConfig.h"
#include "rbnTransitionMat.h"
#include "rbnStateSetUtils.h"
#include "rbnNetworkAlg.h"


bool testMatrixFunctionallity0()
{
    using Generator = rbn::StateSetUtils::Types::UniformINTGenerator;

    for(size_t tSz = 0; tSz < 20; tSz++) {

        rbn::TransitionMat network(tSz);
        std::vector<size_t> sample(1 << tSz);

        Generator gen = rbn::StateSetUtils::getUnformIntGen(network.nRows() - 1);

        // Fill with random values
        size_t randomNum = 0;

        for (size_t i = 0, sz = network.nRows(); i < sz; i++) {
            randomNum = gen();
            network[i] = randomNum;
            sample[i] = randomNum;
        }

        // Test operator[]
        for (size_t i = 0, sz = network.nRows(); i < sz; i++) {
            if(!(sample[i] == network[i])) {
                return false;
            }
        }
    }
    return true;
}

bool testMatrixFunctionallity1()
{
    rbn::TransitionMat table(1);
    table.resizeTable(2);
    table[0][1] = 1;
    table[3][0] = 1;

    if(table[0][1] != 1) {
        return false;
    }

    if(table[3][0] != 1) {
        return false;
    }

    if(table.nCols() != 2) {
        return false;
    }

    if(table.nRows() != 4) {
        return false;
    }

    table.clearTable();
    if(table[0][1] != 0) {
        return false;
    }

    if(table[3][0] != 0) {
        return false;
    }

    return true;
}

bool testConn0()
{
    rbn::TransitionMat table({
                                 {0, 0, 1},
                                 {0, 1, 0},
                                 {0, 1, 1},
                                 {1, 0, 0},
                                 {1, 0, 1},
                                 {1, 1, 0},
                                 {1, 1, 1},
                                 {0, 0, 0},
                             });




    if(table[6] != 7) {
        return false;
    }
    if((rbn::BNAlgorithms::sequential::averageConnectivity(table) - 2) >= 1e-7) {
        return false;
    }
    if((rbn::BNAlgorithms::sequential::averageBias(table) - 0.5) >= 1e-7 ) {
        return false;
    }
    if((rbn::BNAlgorithms::sequential::averageAttractorLenAndCnt(table).first - 8) != 0) {
        return false;
    }
    if((rbn::BNAlgorithms::sequential::averageAttractorLenAndCnt(table).second - 1) != 0) {
        return false;
    }

    return true;
}

bool testConn1()
{
    rbn::TransitionMat table(3);
    table.setBoolFunction({0, 0, 0, 1, 0, 0, 0, 0}, 0);
    table.setBoolFunction({1, 1, 0, 0, 0, 0, 0, 0}, 1);
    table.setBoolFunction({0, 0, 0, 0, 0, 0, 0, 0}, 2);

    double conn = rbn::BNAlgorithms::sequential::averageConnectivity(table);
    return std::abs(conn - 5.0 / 3) <= 1e-7;
}

bool testConn2()
{
    rbn::TransitionMat table(4);
    table.setBoolFunction({1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0}, 0);
    table.setBoolFunction({0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 1);
    table.setBoolFunction({0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0}, 2);
    table.setBoolFunction({0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0}, 3);

    double conn = rbn::BNAlgorithms::sequential::averageConnectivity(table);
    return std::abs(conn - 4) <= 1e-7 ;
}


bool makeTests()
{
    bool flag = 1;
    flag = flag && testMatrixFunctionallity0();
    flag = flag && testMatrixFunctionallity1();

    flag = flag && testConn0();
    flag = flag && testConn1();
    flag = flag && testConn2();

    return flag;
}

int main()
{
    std::cout << !makeTests();
    return !makeTests();
}
