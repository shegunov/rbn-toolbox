#include <iostream>
#include <cassert>

#include "rbnConfig.h"
#include "rbnStateSetUtils.h"

bool testStateSetFunctionallity0()
{
    uint8_t power = rbn::StateSet::maxNodesToEncode();

    size_t testNum = (size_t(1) << power) - 1;
    size_t stateSetNum = 0;

    rbn::StateSet xxx(power, testNum);

    for(size_t i = 0; i < power; i++) {
        stateSetNum = xxx;
        if(!(stateSetNum == testNum)) {
            return false;
        }
        xxx[i] = 0;
        testNum >>= 1;
    }
    return true;
}

bool testStateSetFunctionallity1()
{
    uint8_t power = rbn::StateSet::maxNodesToEncode();

    size_t maxEncodedNumber = (size_t(1) << power) - 1;

    rbn::StateSet xxx(power);

    xxx = maxEncodedNumber;
    if(!(xxx == maxEncodedNumber))
        return false;


    for (size_t i = (size_t(1) << 25); i > 0; i--) {
        xxx = i;
        if(!(i == xxx)) {
            return false;
        }
    }

    xxx = 0;
    if (!(xxx == 0)) {
        return false;
    }

    return true;
}

int main()
{
    std::cout << !(testStateSetFunctionallity0() && testStateSetFunctionallity1());
    return !(testStateSetFunctionallity0() && testStateSetFunctionallity1());
}
