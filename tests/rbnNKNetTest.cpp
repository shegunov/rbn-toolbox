#include <iostream>
#include <cassert>

#include "rbnNKModel.h"
#include "rbnTransitionMat.h"

using namespace rbn;

bool testCtor()
{
    const size_t numNodes = 3;
    const size_t maxArgs = 2;

    NKNetwork network(numNodes, maxArgs);

    assert(network.nNodes() == numNodes);

    for (size_t fun = 0; fun < numNodes; ++fun) {
        assert(network.nodeTruthTable(fun).numOfArgs() <= maxArgs);
        if (network.nodeTruthTable(fun).numOfArgs() > maxArgs) return false;
    }

    return true;
}

NKNetwork generateBySample()
{
    NKNetwork net(3, 0);

    net.setTopology({{0, 1, 2}, {1, 2}, {2}});
    net.setBooleanTable(0, BoolFunction({false, false, false, true, true, true, true, false}));
    net.setBooleanTable(1, BoolFunction({false, true, true, false}));
    net.setBooleanTable(2, BoolFunction({true, false}));

    return net;
}

bool testNetwork1()
{
    NKNetwork net = generateBySample();

    assert(net.nodeTruthTable(0).numOfArgs() == 3);
    assert(net.nodeTruthTable(0).calculate(0) == false);
    assert(net.nodeTruthTable(0).calculate(1) == false);
    assert(net.nodeTruthTable(0).calculate(2) == false);
    assert(net.nodeTruthTable(0).calculate(3) == true);
    assert(net.nodeTruthTable(0).calculate(4) == true);
    assert(net.nodeTruthTable(0).calculate(5) == true);
    assert(net.nodeTruthTable(0).calculate(6) == true);
    assert(net.nodeTruthTable(0).calculate(7) == false);

    assert(net.nodeTruthTable(1).numOfArgs() == 2);
    assert(net.nodeTruthTable(1).calculate(0) == false);
    assert(net.nodeTruthTable(1).calculate(1) == true);
    assert(net.nodeTruthTable(1).calculate(2) == true);
    assert(net.nodeTruthTable(1).calculate(3) == false);

    assert(net.nodeTruthTable(2).numOfArgs() == 1);
    assert(net.nodeTruthTable(2).calculate(0) == true);
    assert(net.nodeTruthTable(2).calculate(1) == false);

    for (size_t state = 0; state < 8; ++state) {
        assert (net[state] == (state+1) % 8);
        if (net[state] != (state+1) % 8) return false;
    }

    return true;
}


bool testTransformToTable()
{
    NKNetwork net = generateBySample();
    TransitionMat mat(net);
    assert (mat.nCols() == 3);
    assert (mat.nRows() == 8);

    assert(mat[0] == 1);
    assert(mat[5] == 6);
    assert(mat[7] == 0);
    
    return true;
}


bool makeTests()
{
    bool flag = true;

    flag = flag & testCtor();
    flag = flag & testNetwork1();
    flag = flag & testTransformToTable();

    return flag;
}

int main()
{
    std::cout  << !makeTests();
    return !makeTests();
}
