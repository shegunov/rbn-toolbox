#include <cassert>
#include <cmath>

#include "rbnTransitionMat.h"

using std::log2;

namespace rbn {

TransitionMat::TransitionMat(size_t nNodes)
{
    resizeTable(nNodes);
}

TransitionMat::TransitionMat(std::initializer_list<BooleanFuncVals> graphDiagram)
{
    setTable(graphDiagram);
}

TransitionMat::TransitionMat(const std::vector<StateSet> &graphDiagram)
{
    setTable(graphDiagram);
}

TransitionMat::~TransitionMat()
{}

StateSet& TransitionMat::operator[] (size_t index)
{
    return network[index];
}

const StateSet TransitionMat::operator[] (size_t index) const
{
    return network[index];
}

size_t TransitionMat::nRows() const
{
    return network.size();
}

size_t TransitionMat::nCols() const
{
    return network[0].size();
}

void TransitionMat::setTable(std::initializer_list<BooleanFuncVals> graphDiagram)
{
    assert(graphDiagram.size() > 0 &&
           !(graphDiagram.size() & (graphDiagram.size() - 1)));

    network.reserve(graphDiagram.size());
    assert((size_t(1) << size_t(log2(graphDiagram.size()))) == graphDiagram.size());

    for (const BooleanFuncVals& set : graphDiagram) {
        network.emplace_back(StateSet(set));
    }
}
void TransitionMat::setTable(const std::vector<StateSet> &graphDiagram)
{
    network = graphDiagram;
}

void TransitionMat::resizeTable(size_t nNewNodes)
{
    assert(nNewNodes <= StateSet::maxNodesToEncode());

    //Update old part of the table
    for(auto& row : network) {
        row.resize(nNewNodes);
    }

    network.resize(size_t(1) << nNewNodes, StateSet(nNewNodes, 0));
}

void TransitionMat::setBoolFunction(const std::vector<bool>& f, size_t pos)
{
    assert(pos <= nCols() && f.size() == network.size());

    size_t mask = 1 << ( nCols() - 1 - pos);
    size_t set = 0;
    for (size_t i = 0, sz = network.size(); i < sz; i++) {
        set = network[i];
        set = (set & ~mask) | (-f[i] & mask);
        network[i] = set;
    }
}

void TransitionMat::clearTable()
{
    for (size_t i = 0, sz = network.size(); i < sz; i++) {
        network[i] = 0;
    }
}

std::vector<std::vector<bool>> TransitionMat::toVectorTable() const
{
    std::vector<std::vector<bool>> table(nRows(), std::vector<bool>(nCols(), 0));
    for (size_t i = 0, rows = nRows(); i < rows; i++) {
        for (size_t j = 0, cols  = nCols(); j < cols; j++) {
            table[i][j] = (*this)[i][j];
        }
    }
    return table;
}
} // namespace rbn
