#ifndef RBN_NETWORK_ALGORITHMS_INCLUDED
#define RBN_NETWORK_ALGORITHMS_INCLUDED


#include <cstdlib>
#include <vector>

#include "rbnConfig.h"
#include "rbnNetwork.h"

#ifdef WITH_THREADS
#include <thread>
#endif

namespace rbn {


struct BNAlgorithms
{
    struct sequential
    {
        static size_t fillWithRandom(BooleanNetwork& network);
        static size_t fillRandom(BooleanNetwork& network, size_t maxConn);
        static double averageConnectivity(const BooleanNetwork& network);
        static double averageBias(const BooleanNetwork& network);
        static std::pair<size_t, size_t> averageAttractorLenAndCnt(const BooleanNetwork& network);
    };
#ifdef WITH_THREADS
    struct parallel
    {
        static size_t fillWithRandom(BooleanNetwork& network, size_t numOfThreads = std::thread::hardware_concurrency());
        static size_t fillRandom(BooleanNetwork& network, size_t maxConn, size_t numOfThreads = std::thread::hardware_concurrency());
        static double averageConnectivity(const BooleanNetwork& network, size_t numOfThreads = std::thread::hardware_concurrency());
        static double averageBias(const BooleanNetwork& network, size_t numOfThreads = std::thread::hardware_concurrency());
    };
#endif
};

} // namespace rbn
#endif //RBN_NETWORK_INCLUDED
