#include "rbnNKModel.h"
#include "rbnBoolFuncUtils.h"
#include "rbnTransitionMat.h"

#include <cassert>

namespace rbn {

NKNetwork::NKNetwork() : NKNetwork(0, 0)
{}

NKNetwork::NKNetwork(size_t nodes, size_t maxArgs)
{
    assert (nodes < 60);
    assert (maxArgs <= nodes);

    network.reserve(nodes);
    for (size_t fun = 0; fun < nodes; ++fun) {
        network.emplace_back(BoolFunctionUtils::generateFunction(maxArgs, nodes));
    }
}


const StateSet NKNetwork::operator[] (size_t index) const
{
    const size_t nodes = nNodes();
    const StateSet state(nodes, index);

    size_t networkState = 0;
    for (size_t nodeId = 0; nodeId < nodes; ++nodeId) {
        bool nodeState = BoolFunctionUtils::evalNode(network[nodeId], state);
        networkState |= (size_t(nodeState) << (nodes - nodeId -1));
    }

    return StateSet(nodes, networkState);
}


void NKNetwork::setTopology(const std::vector<std::vector<size_t>>& topology)
{
    network.clear();
    network.reserve(topology.size());
    for (size_t iNode = 0; iNode < topology.size(); ++iNode) {
        const BoolFunction function =
            BoolFunctionUtils::getRandomBoolVector(size_t(1)<<topology[iNode].size());
        const NKNode node = {topology[iNode], function};
        network.emplace_back(node);
    }
}


void NKNetwork::setBooleanTable(size_t nodeId, const BoolFunction& fun)
{
    assert (nodeId < nNodes());
    assert (fun.numOfArgs() == network[nodeId].second.numOfArgs());

    network[nodeId].second = fun;
}


const BoolFunction& NKNetwork::nodeTruthTable(size_t nodeId) const
{
    assert (nodeId < nNodes());
    return network[nodeId].second;
}


size_t NKNetwork::nNodes() const
{
    return network.size();
}


NKNetwork::operator TransitionMat() const
{
    const size_t numStates = size_t(1) << nNodes();
    std::vector<StateSet> states;
    states.reserve(numStates);

    for (size_t curState = 0; curState < numStates; ++curState) {
        states.emplace_back((*this)[curState]);
    }

    return TransitionMat(states);
}

size_t NKNetwork::nRows() const
{
    return 1 << nNodes();
}

size_t NKNetwork::nCols() const
{
    return  nNodes();
}

std::vector<NKNode>& NKNetwork::getNetwork()
{
    return network;
}

} // namespace rbn
