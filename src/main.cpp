#include <iostream>
#include <fstream>
#include <cassert>

#include "rbnConfig.h"
#include "rbnTransitionMat.h"
#include "rbnNetworkAlg.h"
#include "rbnSettings.h"

#include "perfCounter.h"

void reportVerssion(int, char** argv)
{
    // report version
    std::cout
            << argv[0]
            << " Version: "
            << RBN_VERSION_MAJOR << "."
            << RBN_VERSION_MINOR
            << std::endl;
}


void runTestOnMultiThreads(
    size_t tableSize, size_t numTasks,
    int numThreads, const char* outputFile);

rbn::utils::Settings readSettings(const std::string& file);

int main(int argc, char** argv)
{
    reportVerssion(argc, argv);
    try {
        rbn::utils::Settings config;
        if (argc < 2) {
            config = readSettings("config.ini");
        }
        else {
            config = readSettings(argv[1]);
        }

        clearPerformance();
        {
            countPerformance();
            runTestOnMultiThreads(
                        config["nGanes"],
                        config["nSamples"],
                        size_t(config["nThreads"]),
                        std::string(config["output"]).c_str()
                    );
        }

        if (std::string(config["perfOutput"]).size()) {
            std::ofstream perfOutput(config["perfOutput"]);
            if (perfOutput.is_open() && perfOutput.good()) {
                rbn::utils::PerfMetrics::getInstance().printStatistics(perfOutput);
            }
            perfOutput.close();
        }

    } catch (const std::exception& e) {
        std::cerr << e.what() << std::endl;
        return 1;
    }

    return 0;
}

