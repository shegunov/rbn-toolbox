#ifndef RBN_BOOL_FUNCTION_INCLUDED
#define RBN_BOOL_FUNCTION_INCLUDED

#include <vector>

#include "rbnStateSet.h"

namespace rbn {

struct BoolFunction
{
public:

    BoolFunction(const std::vector<bool> &truthTable = std::vector<bool>());

    BoolFunction(const BoolFunction &) = default;
    BoolFunction& operator= (const BoolFunction &) = default;
    ~BoolFunction() = default;

    void setTable(const std::vector<bool> &table);
    size_t numOfArgs() const;

    bool calculate(size_t argumentList) const;

private:
    size_t nArgs;
    std::vector<bool> truthTable;
};


} // End namesapce rbn
#endif // RBN_BOOL_FUNCTION_INCLUDED
