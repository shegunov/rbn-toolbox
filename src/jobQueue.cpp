#include "jobQueue.h"

JobQueue::JobQueue(int threads)
    : threadManager(threads), executor(*this), isRunning(false)
{
}


JobQueue::~JobQueue()
{
    assert(!isRunning && "JobQueue must be stopped before destructor");
}


void JobQueue::start()
{
    isRunning = true;
    threadManager.start();
    threadManager.runThreadsNoWait(executor);
}


void JobQueue::stop()
{
    isRunning = false;
    haveWorkEvent.notify_all();
    threadManager.stop();
}


void JobQueue::addJob(const Job &job)
{
    {
        std::lock_guard<std::mutex> lock(workMtx);
        jobs.push(job);
    }

    haveWorkEvent.notify_one();
}


void JobQueue::waitDone()
{
    std::unique_lock<std::mutex> lock(workMtx);
    if (unlockedRemainingJobs() == 0) {
        return;
    }

    doneWorkEvent.wait(lock, [this]() {
        return unlockedRemainingJobs() == 0;
    });
}


int JobQueue::unlockedRemainingJobs() const
{
    return activeWorkers + int(jobs.size());
}


JobQueue::JobExecutor::JobExecutor(JobQueue &queue)
    : queue(queue)
{
}


void JobQueue::JobExecutor::run(int, int)
{
    while (queue.isRunning) {
        Job current;
        bool haveJob = false;
        if (!queue.jobs.empty()) {
            std::unique_lock<std::mutex> lock(queue.workMtx);
            queue.haveWorkEvent.wait(lock, [this] {
                return !queue.jobs.empty() || !queue.isRunning;
            });

            if (queue.isRunning) {
                haveJob = true;
                current = queue.jobs.front();
                queue.jobs.pop();
                ++queue.activeWorkers;
            }
        }

        if (queue.isRunning && haveJob) {
            current();
            bool signalDone = false;
            {
                std::lock_guard<std::mutex> lock(queue.workMtx);
                --queue.activeWorkers;
                signalDone = queue.activeWorkers == 0;
            }

            if (signalDone) {
                queue.doneWorkEvent.notify_all();
            }
        }
    }
}
