#include <cassert>

#include "rbnStateSet.h"


namespace rbn {


StateSet::StateSet(size_t stateSz, size_t state)
{
    assert(stateSz <= distance());
    this->state = (size_t(stateSz) << distance()) + state;
}

StateSet::StateSet(std::initializer_list<bool> l)
{
    assert(l.size() <= distance());

    uint8_t sz = uint8_t(l.size());

    state = size_t(sz) << distance();

    size_t cnt = 0;
    for (const auto& val : l) {
        state += (size_t(val) << (sz - 1 - cnt));
        ++cnt;
    }
}

StateSet& StateSet::operator=(size_t state)
{
    uint8_t sz = size();
    assert(state < (size_t(1) << sz));
    *this = StateSet(sz, state);
    return *this;
}

StateSet::operator size_t() const
{
    return state & ~mask();
}

StateSet::StateSetProxy
StateSet::operator[] (size_t index)
{
    assert(index < size());
    return StateSetProxy(state, index, size());
}

bool
StateSet::operator[] (size_t index) const
{
    assert(index < size());
    return state & 1 << (size() - 1 - index);
}

uint8_t StateSet::size() const
{
    size_t sz = state & mask();
    sz >>= distance();
    return uint8_t(sz);
}

constexpr uint8_t StateSet::numOfEncodeBits()
{
    return 6u;
}

constexpr size_t StateSet::mask()
{
    return ((size_t(1) << numOfEncodeBits()) - 1) << distance();
}

constexpr uint8_t StateSet::distance()
{
    return 8 * sizeof(state) - numOfEncodeBits();
}

uint8_t StateSet::maxNodesToEncode()
{
    return distance();
}

void StateSet::resize(size_t stateSz)
{
    assert(stateSz <= distance());
    state = (state & ~mask()) + (size_t(stateSz) << distance());
}

StateSet::StateSetProxy::StateSetProxy(size_t &state, size_t index, size_t size)
    :state(state),index(index), size(size) {}

void StateSet::StateSetProxy::operator=(bool funcVal)
{

    size_t mask = (size_t(1) << ((size - 1) - index));
    state = (state & ~mask) | (-funcVal & mask);
}

StateSet::StateSetProxy::operator bool () const
{
    size_t mask = size_t(1) << ((size - 1) - index);
    return state & mask;
}


} // namespace rbn
