#include "rbnBoolFuncUtils.h"
#include "rbnStateSetUtils.h"

#include <cassert>
#include <set>

namespace rbn {

std::vector<bool> BoolFunctionUtils::getRandomBoolVector(size_t size)
{
    static StateSetUtils::Types::BernoulliGenerator bGen = StateSetUtils::getBernoulliGen();

    std::vector<bool> result;
    result.reserve(size);
    for (size_t i = 0; i < size; ++i) {
        result.emplace_back(bGen());
    }
    return result;
}

NKNode BoolFunctionUtils::generateFunction(size_t args, size_t nodesInNetwork)
{
    static StateSetUtils::Types::UniformINTGenerator iGen = StateSetUtils::getUnformIntGen(nodesInNetwork - 1);
    assert (args <= nodesInNetwork);

    args = iGen() % (args+1);
    NKNode node;

    std::set<size_t> preds;
    while (preds.size() < args) {
        size_t pred = iGen();
        preds.insert(pred);
    }

    node.first.assign(preds.begin(), preds.end());
    node.second.setTable(getRandomBoolVector(size_t(1)<<args));

    return node;
}


// Converts full network state to boolean function argument value
//by extracting values of only connected nodes
bool BoolFunctionUtils::evalNode(const NKNode& node, const StateSet& state)
{
    size_t argValues = 0;
    const size_t size = node.first.size();

    for (size_t arg = 0; arg < size; ++arg) {
        size_t predIdx = node.first[arg];
        if (state[predIdx]) {
            argValues |= (1 << (size - arg - 1));
        }
    }
    return node.second.calculate(argValues);
}

} //namespace rbn
