#ifndef RBN_NK_MODEL_INCLUDED
#define RBN_NK_MODEL_INCLUDED

#include <vector>

#include "rbnNetwork.h"
#include "rbnBoolFunction.h"

namespace rbn {

typedef std::pair<std::vector<size_t>, BoolFunction> NKNode;

class TransitionMat;

class NKNetwork
        : public BooleanNetwork
{
public:
    NKNetwork(size_t nodes, size_t maxArgs);
    NKNetwork();
    ~NKNetwork() override = default;

    NKNetwork(const NKNetwork&) = delete;
    NKNetwork(NKNetwork&&) = default;
    NKNetwork& operator=(const NKNetwork&) = delete;
    NKNetwork& operator=(NKNetwork&&) = default;

    virtual const StateSet operator[] (size_t index) const override;

    void setTopology(const std::vector<std::vector<size_t>>& topology);
    void setBooleanTable(size_t nodeId, const BoolFunction& fun);

    const BoolFunction& nodeTruthTable(size_t nodeID) const;

    size_t nNodes() const;
    size_t nRows() const override;
    size_t nCols() const override;


    operator TransitionMat() const;

    std::vector<NKNode>& getNetwork();

private:
    std::vector<NKNode> network;
};

} // namespace rbn

#endif //RBN_NK_MODEL_INCLUDED
