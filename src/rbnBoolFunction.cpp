#include <cassert>
#include <cmath>

#include "rbnBoolFunction.h"

using namespace rbn;
using std::vector;
using std::log2;

namespace rbn {

BoolFunction::BoolFunction(const vector<bool> &truthTable)
{
    setTable(truthTable);
}

void BoolFunction::setTable(const vector<bool> &table)
{
    // Check if it is power of two or empty truthTable
    assert(!(table.size() & (table.size() - 1)));
    truthTable = table;
    nArgs = truthTable.size() ? size_t(log2(truthTable.size())) : 0;
}


size_t BoolFunction::numOfArgs() const
{
    return nArgs;
}


bool BoolFunction::calculate(size_t argumentList) const
{
    assert(argumentList < truthTable.size());
    return truthTable[argumentList];
}

} // namespace rbn
