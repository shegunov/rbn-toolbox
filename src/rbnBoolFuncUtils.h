#ifndef RBN_BOOL_FUNC_UTILS_INCLUDED
#define RBN_BOOL_FUNC_UTILS_INCLUDED

#include "rbnNKModel.h"

namespace rbn {
class BoolFunctionUtils
{
public:
    static std::vector<bool> getRandomBoolVector(size_t size);
    static NKNode generateFunction(size_t args, size_t nodesInNetwork);
    static std::vector<size_t> getSignificantArgs(const BoolFunction& function);
    static bool evalNode(const NKNode&, const StateSet& state);
};

} //namespace rbn

#endif //RBN_BOOL_FUNC_UTILS_INCLUDED
