#include "rbnSettings.h"
#include <cassert>

namespace rbn {
namespace utils {

Settings::SettingsVariant::SettingsVariant()
    :type(Settings::SettingsVariant::VARIANT::NONE)
{ }

Settings::SettingsVariant::SettingsVariant(size_t asInt)
    :type(Settings::SettingsVariant::VARIANT::INT)
{
    this->asSize_t = asInt;
}

Settings::SettingsVariant::SettingsVariant(double asDouble)
    :type(Settings::SettingsVariant::VARIANT::FLOAT)
{
    this->asDouble = asDouble;
}

Settings::SettingsVariant::SettingsVariant(const std::string& asString)
    :type(Settings::SettingsVariant::VARIANT::STRING)
{
    new (&this->asString) std::string();
    this->asString = asString;
}

Settings::SettingsVariant::SettingsVariant(const SettingsVariant& other)
{
    type = other.type;
    switch (other.type) {
    case VARIANT::NONE:
        break;
    case VARIANT::INT:
        asSize_t = other.asSize_t;
        break;
    case VARIANT::FLOAT:
        asDouble = other.asDouble;
        break;
    case VARIANT::STRING:
        new (&asString) std::string();
        asString = other.asString;
        break;
    }
}

Settings::SettingsVariant& Settings::SettingsVariant::operator= (const SettingsVariant& other)
{
    if (&other == this) {
        return *this;
    }

    if (type != VARIANT::NONE) {
        assert(type == other.type);
    }

    destroyValue();
    type = other.type;
    switch (other.type) {
    case VARIANT::NONE:
        break;
    case VARIANT::INT:
        asSize_t = other.asSize_t;
        break;
    case VARIANT::FLOAT:
        asDouble = other.asDouble;
        break;
    case VARIANT::STRING:
        new (&asString) std::string();
        asString = other.asString;
        break;
    }
    return *this;
}

void Settings::SettingsVariant::destroyValue()
{
    switch (type) {
    case VARIANT::NONE:
        break;
    case VARIANT::INT:
        asSize_t = 0;
        break;
    case VARIANT::FLOAT:
        asDouble = 0.0;
        break;
    case VARIANT::STRING:
        asString.~basic_string();
        break;
    }
}

Settings::SettingsVariant::operator std::string() const
{
    assert(type == VARIANT::STRING);
    return asString;
}

Settings::SettingsVariant::operator size_t() const
{
    assert(type == VARIANT::INT);
    return asSize_t;
}

Settings::SettingsVariant::operator double() const
{
    assert(type == VARIANT::FLOAT);
    return asDouble;
}

Settings::SettingsVariant::~SettingsVariant()
{
    destroyValue();
}

Settings::SettingsProxy::SettingsProxy(std::map<std::string, SettingsVariant>& options, const std::string& key)
    :options(options), key(key)

{}


void Settings::SettingsProxy::operator=(const  SettingsVariant& value)
{
    options[key] = value;
}

Settings::SettingsProxy Settings::operator[] (const std::string& key)
{
    return SettingsProxy(options, key);
}

Settings::SettingsVariant Settings::operator[] (const std::string& key) const
{
    std::map<std::string, SettingsVariant>::const_iterator it;
    it = options.find(key);
    if (it == options.end()) {
        throw std::runtime_error("Key not found in options");
    }
    return it->second;
}

Settings Settings::getClass(const std::string& keyClass) const
{
    std::map<std::string, SettingsVariant>::const_iterator nextElem;
    nextElem = options.lower_bound(keyClass);
    Settings optsClass;

    while (nextElem != options.end() && nextElem->first.compare(0, keyClass.size(), keyClass) == 0 ) {
        optsClass[nextElem->first] = nextElem->second;
        nextElem++;
    }

    return optsClass;
}

void Settings::SettingsProxy::operator= (size_t value)
{
    SettingsProxy::operator=(SettingsVariant(value));
}

void Settings::SettingsProxy::operator= (double value)
{
    SettingsProxy::operator=(SettingsVariant(value));
}

void Settings::SettingsProxy::operator= (const std::string& value)
{
    SettingsProxy::operator=(SettingsVariant(value));
}

bool Settings::hasKey(const std::string& key)
{
    return !(options.find(key) == options.end());
}

Settings::SettingsProxy::Types Settings::SettingsProxy::type() const
{
    return options[key].type;
}


void Settings::updateValueFromStr(SettingsProxy variant, const std::string &in)
{

    if (variant.type() == SettingsProxy::Types::INT) {
        size_t toInt = size_t(std::stoull(in));
        SettingsVariant res(toInt);
        variant = res;
    }
    else if (variant.type() == SettingsProxy::Types::FLOAT) {
        double toDouble = size_t(std::stod(in));
        SettingsVariant res(toDouble);
        variant = res;
    }

    else if (variant.type() == SettingsProxy::Types::STRING) {
        SettingsVariant res(in);
        variant = res;
    }
    else {
        SettingsVariant bad;
        variant = bad;
    }
}

Settings::SettingsProxy::operator std::string() const
{
    return options[key];
}
Settings::SettingsProxy::operator size_t() const
{
    return options[key];
}

Settings::SettingsProxy::operator double() const
{
    return options[key];
}

} // namespace utils
} // namespace rbn
