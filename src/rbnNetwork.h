#ifndef RBN_NETWORK_INCLUDED
#define RBN_NETWORK_INCLUDED

#include <cstdlib>

#include "rbnStateSet.h"

namespace rbn {


class BooleanNetwork
{
public:
    BooleanNetwork() = default;

    BooleanNetwork(const BooleanNetwork&) = delete;
    BooleanNetwork(BooleanNetwork&&) = default;

    BooleanNetwork& operator=(const BooleanNetwork&) = delete;
    BooleanNetwork& operator=(BooleanNetwork&&) = default;

    virtual ~BooleanNetwork() = default;

    virtual const StateSet operator[] (size_t index) const = 0;

    virtual size_t nRows() const = 0;
    virtual size_t nCols() const = 0;

};

} // namespace rbn
#endif //RBN_NETWORK_INCLUDED
