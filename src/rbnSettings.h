#ifndef RBN_SETTINGS_INCLUDED
#define RBN_SETTINGS_INCLUDED
#include <map>
#include <vector>
#include <cstring>
#include <iostream>
#include <type_traits>

namespace rbn {
namespace utils {
class Settings
{
private:

    struct SettingsProxy;

    struct SettingsVariant
    {
    private:
        friend SettingsProxy;
    public:
        SettingsVariant();
        SettingsVariant(size_t asInt);
        SettingsVariant(double asDobule);
        SettingsVariant(const std::string& asString);

        SettingsVariant(const SettingsVariant& other);
        SettingsVariant(SettingsVariant&& other) = delete;

        SettingsVariant& operator= (const SettingsVariant& other);
        SettingsVariant& operator= (SettingsVariant&& other) = delete;

        operator std::string() const;
        operator size_t() const;
        operator double() const;


        ~SettingsVariant();
    private:
        enum VARIANT {NONE, INT, FLOAT, STRING};

        union
        {
            size_t asSize_t;
            double asDouble;
            std::string asString;
        };

        VARIANT type;
        void destroyValue();
    };

    struct SettingsProxy
    {
    private:
        friend Settings;
        using Types = SettingsVariant::VARIANT;
    public:

        SettingsProxy(std::map<std::string, SettingsVariant>& options, const std::string& key);

        void operator= (size_t value);
        void operator= (double value);
        void operator= (const std::string& value);
        void operator= (const SettingsVariant& value);

        explicit operator SettingsVariant() const;
        operator std::string() const;
        operator size_t() const;
        operator double() const;


    private:
        SettingsProxy::Types type() const;
        std::map<std::string, SettingsVariant>& options;
        const std::string key;
    };


public:

    Settings() = default;
    ~Settings() = default;

    SettingsProxy operator[] ( const std::string& key);
    SettingsVariant operator[] (const std::string& key) const noexcept(false);

    bool hasKey(const std::string& key);

    Settings getClass(const std::string& keyClass) const;

    static void updateValueFromStr(SettingsProxy variant, const std::string &in);

private:
    std::map<std::string, SettingsVariant> options;
};


} // namespace utils
} // namespace rbn

#endif //RBN_SETTINGS_INCLUDED
