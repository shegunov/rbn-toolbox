#include "threadManager.h"

#include <cassert>

ThreadManager::ThreadManager(int threadCount) 
    : count(threadCount)
{}

void ThreadManager::start()
{
    assert(count > 0 && "Task count must be positive");
    assert(threads.size() == 0 && "Already started");

    running = true;
    currentTask.resize(count, nullptr);

    threads.reserve(count);
    for (int c = 0; c < count; c++) {
        threads.emplace_back(&ThreadManager::threadBase, this, c);
    }
}

void ThreadManager::runThreadsNoWait(Task &task)
{
    {
        std::lock_guard<std::mutex> lock(workMtx);
        assert(unlockedAllTasksDone() && "Already working");

        for (int c = 0; c < int(currentTask.size()); c++) {
            currentTask[c] = &task;
        }

        // inside the lock to ensure this is called before Task::run
        task.onBeforeRun(int(threads.size()));
    }

    workEvent.notify_all();
}

void ThreadManager::runThreads(Task &task)
{
    runThreadsNoWait(task);

    // block until task is complete
    if (!unlockedAllTasksDone()) {
        std::unique_lock<std::mutex> lock(workMtx);
        if (!unlockedAllTasksDone()) {
            workEvent.wait(lock, [this]() {
                return unlockedAllTasksDone();
            });
        }
    }
}

void ThreadManager::stop()
{
    assert(running && "Can't stop if not running");

    {
        std::lock_guard<std::mutex> lock(workMtx);
        running = false;
    }

    workEvent.notify_all();

    // joining threads will implicitly wait for all of them to finish current task
    for (size_t c = 0; c < threads.size(); c++) {
        threads[c].join();
    }

    threads.clear();
    currentTask.clear();
}

size_t ThreadManager::getThreadCount() const
{
    return threads.size();
}

void ThreadManager::threadBase(volatile int threadIndex)
{
    while (true) {
        Task *toExecute = nullptr;
        if (running) {
            std::unique_lock<std::mutex> lock(workMtx);

            if (running && currentTask[threadIndex] == nullptr) {
                workEvent.wait(lock, [this, threadIndex]() {
                    return currentTask[threadIndex] != nullptr || !running;
                });
            }

            // just copy, and do not clear the value in @currentTask
            // it is used to signal the task is completed
            toExecute = currentTask[threadIndex];
        }

        if (!running) {
            return;
        }

        assert(toExecute);

        toExecute->run(threadIndex, int(threads.size()));
        {
            std::lock_guard<std::mutex> lock(workMtx);
            //remove the task from the queue
            currentTask[threadIndex] = nullptr;
        }

        // Since start and finish share an event this must wake all threads
        // to make sure the caller is awoken and not only some other worker
        workEvent.notify_all();
    }
}

bool ThreadManager::unlockedAllTasksDone() const 
{
    for (size_t c = 0; c < currentTask.size(); c++) {
        if (currentTask[c]) {
            return false;
        }
    }
    return true;
}


void Task::runOn(ThreadManager &tm)
{
    tm.runThreads(*this);
}
