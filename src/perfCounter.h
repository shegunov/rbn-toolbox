#ifndef PERFORMANCE_COUNTER_INCLUDED
#define PERFORMANCE_COUNTER_INCLUDED
#ifdef PERFCOUNT
#include <cstring>
#include <map>
#include <chrono>
#include <iostream>

namespace rbn {
namespace utils {

class PerfMetrics
{
    PerfMetrics(const PerfMetrics&) = delete;
    void operator= (const PerfMetrics&) = delete;

public:

    using string = std::string;
    using secondsUnit = std::chrono::microseconds;

    static PerfMetrics& getInstance();
    void printStatistics(std::ostream& stream = std::cout) const;

    void registerFunctionTime(const string& funtion, const secondsUnit& time);
    void resetStatistics();
    void clear();

private:
    PerfMetrics() = default;
    ~PerfMetrics() = default;

private:
    std::map<string, secondsUnit> statistics;
};

struct PerfCounter
{
    PerfCounter(const PerfCounter&) = delete;
    void operator=(const PerfCounter&) = delete;

public:
    PerfCounter(const std::string& function);
    ~PerfCounter();
    
private:
    std::string function;
    std::chrono::system_clock::time_point start;
};

} // namespace utils
} // namespace rbn

#define countPerformance() rbn::utils::PerfCounter __local_counter(__func__)
#define clearPerformance() rbn::utils::PerfMetrics::getInstance().clear()
#define printPerfStatistics() rbn::utils::PerfMetrics::getInstance().printStatistics()
#else 
#   define countPerformance()
#   define clearPerformance()
#   define printPerfStatistics()
#endif //PERFCOUNT

#endif //PERFORMANCE_COUNTER_INCLUDED
