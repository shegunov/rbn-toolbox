#include <fstream>
#include <algorithm>
#include <thread>
#include <unistd.h>

#include "rbnSettings.h"

void checkForConsistency(rbn::utils::Settings& conf);

rbn::utils::Settings readSettings(const std::string& file)
{
    rbn::utils::Settings config;
    //Fill with default values
    config["networkType"] = "";
    config["output"] = "";
    config["perfOutput"] = "";
    config["nThreads"] = size_t(0);
    config["nGanes"] = size_t(0);
    config["nSamples"] = size_t(0);

    if (access(file.c_str(), F_OK | R_OK) != 0) {
        std::invalid_argument("Config file does not exist, or it can't be read");
    }

    std::ifstream iniFile(file);
    if (!iniFile.is_open()) {
        std::invalid_argument("File can't be open");
    }

    auto clearWS = [] (std::string& str) -> std::string& {
        str.erase(remove_if(str.begin(), str.end(), isspace), str.end());
        return str;
    };

    auto toKV = [] (const std::string& str, const std::string& d = "=") -> std::pair<std::string, std::string> {
        size_t pos = str.find(d);
        return pos == std::string::npos ?
                    std::pair<std::string, std::string> () :
                    std::pair<std::string, std::string> (str.substr(0, pos), str.substr(pos + d.length(), str.size() - 1));
    };


    std::string line;
    size_t lineCnt = 0;
    std::pair<std::string, std::string> kv;
    while (std::getline(iniFile, line)) {
        lineCnt++;
        const std::string compressedLine = clearWS(line);

        // Skip empty lines and comments
        if (line.size() == 0 || line[0] == '#') {
            continue;
        }

        kv = toKV(compressedLine);

        // We do not have key=value row
        if (kv.first.size() == 0) {
            throw std::runtime_error("Config file, parse error on line: " + std::to_string(lineCnt));
        }

        if(!config.hasKey(kv.first)) {
            throw std::runtime_error("Config file, parse error on line: " + std::to_string(lineCnt) + " : key value not recognized");
        }

        try {
            auto x = config[kv.first];
            rbn::utils::Settings::updateValueFromStr(config[kv.first], kv.second);

        } catch (const std::exception& e) {
            throw std::runtime_error("Bad value for key: " + kv.first + " on line:" + std::to_string(lineCnt));
        }
    }

    iniFile.close();
    checkForConsistency(config);
    return config;
}

void checkForConsistency(rbn::utils::Settings& conf)
{
    std::vector<std::string> netWorkSupportedTypes = {"TRMAT_NETWORK", "NK_NETWORK"};
    auto stringInArray = [] (const std::string& key, const std::vector<std::string>& vList) -> bool {
        for(const auto& val : vList) {
            if (strcmp(key.c_str(), val.c_str()) == 0) {
                return true;
            }
        }
        return false;
    };

    std::string netWorkType = conf["networkType"];

    //Check supproted types
    if (netWorkType.size() == 0) {
        std::cout << "INFO: Setting network type to defaulted value: TRMAT_NETWORK" << std::endl;
        conf["networkType"] = "TRMAT_NETWORK";
    } else if (!stringInArray(netWorkType, netWorkSupportedTypes)){
        throw  std::runtime_error("networkType not recodnized");
    }

    //Check threads
    if(size_t(conf["nThreads"]) == size_t(0)) {
        std::cout << "INFO: Setting number of threads to maximum supproted." << std::endl;
        conf["nThreads"] = size_t(std::thread::hardware_concurrency());
    } else if (size_t(conf["nThreads"]) > size_t(std::thread::hardware_concurrency())) {
        std::cerr << "WARN: Requested number of threads is bigger then maximum available for the system." << std::endl;
    }

    //Check nGens
    if (size_t(conf["nGanes"]) == 0) {
        throw std::invalid_argument("Number of ganes, must be greater then 0");
    }
    else if(size_t(conf["nGanes"]) > size_t(32)) {
        std::cout << "INFO: Setting number of ganes too hich may lead to crash." << std::endl;
        conf["nThreads"] = size_t(std::thread::hardware_concurrency());
    } else if (size_t(conf["nGanes"]) > size_t(58)) {
        throw std::invalid_argument("Number of ganes, too heigh - maximum supproted: 58");
    }

    //Check nGens
    if (size_t(conf["nSamples"]) == 0) {
        throw std::invalid_argument("Number of samples, must be greater then 0");
    }

    //Check resultsfile
    std::ofstream output(conf["output"]);
    if (!output.is_open() || !output.good()) {
        throw std::invalid_argument("Cannot create output file");
    }

    //Check perfOutput file
    std::string pefoutPut = conf["perfOutput"];
    if (pefoutPut.size()) {
        std::ofstream perfOutput(conf["perfOutput"]);
        if (!perfOutput.is_open() || !perfOutput.good()) {
            throw std::invalid_argument("Cannot create performance output file");
        }
        perfOutput.close();
    }

}
