#include "perfCounter.h"
#ifdef PERFCOUNT
#include <iomanip>

namespace rbn {
namespace utils {

PerfMetrics& PerfMetrics::getInstance()
{
    static PerfMetrics theInstance;
    return theInstance;
}

void PerfMetrics::resetStatistics()
{
    for (const auto& kv : statistics) {
        statistics[kv.first] = secondsUnit::zero();
    }
}

void PerfMetrics::clear()
{
    statistics.clear();
}

void PerfMetrics::registerFunctionTime(const string& function, const secondsUnit& time)
{
    statistics[function] += time;
}

void PerfMetrics::printStatistics(std::ostream& stream) const
{
    for (const auto& kv : statistics) {
        stream << std::setw(30) << kv.first << " : "
               << std::setw(10) << std::right << kv.second.count() << " ms \n";
    }
    stream << std::endl;
}


PerfCounter::PerfCounter(const std::string& fun)
    : function(fun)
    , start(std::chrono::high_resolution_clock::now())
{ }

PerfCounter::~PerfCounter()
{
    PerfMetrics::secondsUnit duration =
            std::chrono::duration_cast<PerfMetrics::secondsUnit> (std::chrono::high_resolution_clock::now() - start);

    PerfMetrics::getInstance().registerFunctionTime(function, duration);
}

} //namespace utils
} //namespace rbn
#endif
