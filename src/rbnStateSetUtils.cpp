#include "rbnStateSetUtils.h"

namespace rbn {

XorGenerator64::XorGenerator64(size_t seed)
    :state(seed){}

size_t XorGenerator64::operator() ()
{
    size_t x = state;
    x ^= x << 13;
    x ^= x >> 7;
    x ^= x << 17;
    return state = x;
}

SSGenerator<XorGenerator64, std::uniform_int_distribution<size_t>>::SSGenerator(size_t seed, size_t maxVal)
    :rng(seed), maxVal(maxVal)
{}

size_t SSGenerator<XorGenerator64, std::uniform_int_distribution<size_t>>::operator()()
{
    return !maxVal ? 0 : rng() % maxVal;
}

StateSetUtils::Types::UniformINTGenerator StateSetUtils::getUnformIntGen(size_t maxVal)
{
    static std::random_device rd;
    return Types::UniformINTGenerator(rd(), maxVal);
}
StateSetUtils::Types::UniformRealGenerator StateSetUtils::getUnformRealGen(double_t maxVal)
{
    static std::random_device rd;
    return Types::UniformRealGenerator(rd(), maxVal);
}

StateSetUtils::Types::BernoulliGenerator StateSetUtils::getBernoulliGen(double prob)
{
    static std::random_device rd;
    return Types::BernoulliGenerator(rd(), prob);
}
} // namespace rbn
