#ifndef RBN_TRANSITION_MATRIX_INCLUDED
#define RBN_TRANSITION_MATRIX_INCLUDED

#include <vector>

#include "rbnNetwork.h"

namespace rbn {


class TransitionMat
        :public BooleanNetwork
{

public:
    using BooleanFuncVals = std::initializer_list<bool>;

    TransitionMat(size_t nNodes = 0);
    TransitionMat(std::initializer_list<BooleanFuncVals> graphDiagram);
    TransitionMat(const std::vector<StateSet>& graphDiagram);

    TransitionMat(const TransitionMat& other) = delete;
    TransitionMat(TransitionMat&& other) = default;

    TransitionMat& operator= (const TransitionMat& other) = delete;
    TransitionMat& operator= (TransitionMat&& other) = default;

    virtual ~TransitionMat() override;

    StateSet& operator[] (size_t index);
    const StateSet operator[] (size_t index) const override;

    size_t nRows() const override;
    size_t nCols() const override;

    void resizeTable(size_t nNodes);

    void clearTable();

    std::vector<std::vector<bool>> toVectorTable() const;

    void setTable(std::initializer_list<BooleanFuncVals> graphDiagram);
    void setTable(const std::vector<StateSet>& graphDiagram);
    void setBoolFunction(const std::vector<bool> &f, size_t pos);

private:
    std::vector<StateSet> network;
};

} // namespace rbn
#endif //RBN_TRANSITION_MATRIX_INCLUDED
