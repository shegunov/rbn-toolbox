#include "syncWriter.h"

#include <iostream>

namespace rbn {
namespace utils {

SyncTextWriter::SyncTextWriter(const char* filename)
    : useStdout (filename && filename[0])
    , file(useStdout ? filename : "", std::ios::trunc)
    , writer(useStdout ? static_cast<std::ostream&>(file) : static_cast<std::ostream&>(std::cout))
{
}

bool SyncTextWriter::writeStringNl(const std::string& text)
{
    std::unique_lock<std::mutex> lock(fileMtx);
    writeStringUnlocked(text);
    return writeStringUnlocked("\n");
}

bool SyncTextWriter::writeStringNl(const char* text)
{
    std::unique_lock<std::mutex> lock(fileMtx);
    writeStringUnlocked(text);
    return writeStringUnlocked("\n");
}

bool SyncTextWriter::writeString(const std::string& text)
{
    std::unique_lock<std::mutex> lock(fileMtx);
    return writeStringUnlocked(text);
}

bool SyncTextWriter::writeString(const char* text)
{
    std::unique_lock<std::mutex> lock(fileMtx);
    return writeStringUnlocked(text);
}

bool SyncTextWriter::isOpen() const
{
    return writer.good();
}

bool SyncTextWriter::writeStringUnlocked(const std::string& str)
{
    return (writer << str).good();
}

bool SyncTextWriter::writeStringUnlocked(const char* str)
{
    return (writer << str).good();
}

} // namespace utils
} // namespace rbn
