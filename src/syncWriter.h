#ifndef SYNC_WRITER_INCLUDED
#define SYNC_WRITER_INCLUDED

#include <fstream>
#include <mutex>

namespace rbn {
namespace utils {

class SyncTextWriter
{
public:
    SyncTextWriter(const char* fileName);

    bool writeStringNl(const std::string& text);
    bool writeStringNl(const char* text);
    bool writeString(const std::string& text);
    bool writeString(const char* text);

    bool isOpen() const;

private:
    bool writeStringUnlocked(const std::string& what);
    bool writeStringUnlocked(const char* what);

private:
    std::mutex fileMtx;
    bool useStdout;
    std::ofstream file;
    std::ostream& writer;
};

} // namespace utils
} // namespace rbn

#endif //SYNC_WRITER_INCLUDED
