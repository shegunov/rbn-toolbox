#ifndef RBN_TRANSITION_MATRIX_FUNCTIONS_UTILS_INCLUDED
#define RBN_TRANSITION_MATRIX_FUNCTIONS_UTILS_INCLUDED

#include <vector>

#include "rbnNetwork.h"
#include "rbnStateSetUtils.h"

namespace rbn {

class NewtowrkUtils
{
public:
    static size_t findAttractor(const BooleanNetwork& network,
                                size_t startNodeId, std::pair<size_t, size_t>& fromTo,
                                std::vector<char> &visited);

    static void findAttractor(const BooleanNetwork& network,
                              size_t startNodeId, std::pair<size_t, size_t>& fromTo,
                              std::vector<char>& visited, size_t& result);

    static double functionBias(const BooleanNetwork& network, size_t funcId);
    static void functionBias(const BooleanNetwork& network, size_t funcId, double& result);


    static void averageConnecivityFromTo(const BooleanNetwork& network,
                                         size_t from, size_t to,
                                         void* params, double& result);
    static void averageBiasFromTo(const BooleanNetwork& network,
                                  size_t from, size_t to,
                                  void* params, double& result);

    static void randomFillerFromTo(const BooleanNetwork& network,
                                   size_t from, size_t to,
                                   void* params, double& result);
};


} // namespace rbn
#endif //RBN_TRANSITION_MATRIX_FUNCTIONS_UTILS_INCLUDED
