#include <cassert>

#include "rbnStateSetUtils.h"
#include "rbnNetworkAlgUtils.h"
#include "perfCounter.h"
#include "rbnNKModel.h"
#include "rbnBoolFuncUtils.h"

namespace rbn {

size_t markAsVisited(const BooleanNetwork& network, size_t from, size_t to, std::vector<char>& visited, char val)
{
    size_t cnt = 0;
    size_t currNode = from;
    while(currNode != to) {
        visited[currNode] = val;
        currNode = network[currNode];
        cnt++;
    }
    visited[to] = val;
    return cnt++;
}

size_t significuntArgCycle(size_t f, const BooleanNetwork& network)
{
    size_t nFunc = network.nCols();
    size_t nStates = network.nRows();

    size_t cnt = 0;
    for (size_t i = 0; i < nFunc; i++) {
        for(size_t j = 0, k = 0, sz = (nStates >> 1), c = 1 << (nFunc - 1 - i); j < sz; j++, k++) {
            k += k && k % c == 0 ? c : 0;
            if (network[k][f] != network[k + c][f]) {
                ++cnt;
                break;
            }
        }
    }

    return cnt;
}

size_t significuntArgXor(size_t fun, const BooleanNetwork& network)
{
    const size_t max = network.nRows();
    const size_t args = network.nCols();
    const size_t funMask = (size_t(1) << fun);

    size_t cnt = 0;

    for (size_t argMask = 1, arg = 0; arg < args; ++arg, argMask <<= 1) {
        for (size_t row = 0; row < max; ++row) {
            if ((network[row] & funMask) != (network[row^argMask] & funMask)) {
                ++cnt;
                break;
            }
        }
    }

    return cnt;
}

size_t NewtowrkUtils::findAttractor(const BooleanNetwork &network,
                                      size_t startNodeId, std::pair<size_t, size_t>& fromTo,
                                      std::vector<char>& visited)
{
    enum {CANDIDATE = 1, VISITED = 2};
    size_t sz = visited.size();

    size_t curNode = startNodeId;
    // Mark as visited
    visited[curNode] = true;
    for (size_t i = 0; i < sz; i++) {
        // We are on a node that leads to already found attractor
        if (visited[network[curNode]] == VISITED) {
            markAsVisited(network, startNodeId, curNode, visited, VISITED);
            return 0;
        }
        // We are at attractor
        else if (visited[network[curNode]] == CANDIDATE) {
            // Mark the chain as attractor
            fromTo.first = network[curNode];
            fromTo.second = curNode;
            markAsVisited(network, startNodeId, curNode, visited, VISITED);
            return markAsVisited(network, network[curNode], fromTo.second, visited, VISITED) + 1;

        } else {
            //Mark as visited
           visited[network[curNode]] = CANDIDATE;
           curNode = network[curNode];
        }
    }

    return 0;
}

void NewtowrkUtils::findAttractor(const BooleanNetwork &network,
                                      size_t startNodeId, std::pair<size_t, size_t>& fromTo,
                                      std::vector<char> &visited, size_t& result)
{
    result = findAttractor(network, startNodeId, fromTo, visited);
}

double NewtowrkUtils::functionBias(const BooleanNetwork& network, size_t funcId)
{
    double bias = 0.0;
    size_t sz = network.nRows();
    for (size_t i = 0; i < sz; i++)
        bias += network[i][funcId] == 0;

    return bias / sz;
}

void NewtowrkUtils::functionBias(const BooleanNetwork& network, size_t funcId, double& result)
{
    result = functionBias(network, funcId);
}

void NewtowrkUtils::averageConnecivityFromTo(
        const BooleanNetwork& network, size_t from, size_t to,
        void*, double& result)
{
    size_t sum = 0;
    for (size_t i = from; i < to; ++i) {
        sum  += significuntArgCycle(i, network);
    }
    result = double(sum) / network.nCols();
}

void NewtowrkUtils::averageBiasFromTo(
        const BooleanNetwork& network, size_t from, size_t to,
        void*, double& result)
{
    double accumulator = 0.0;
    for (size_t i = from; i < to; ++i) {
        accumulator += functionBias(network, i);
    }
    result =  accumulator / network.nCols();
}

void NewtowrkUtils::randomFillerFromTo(
        const BooleanNetwork& network, size_t from, size_t to,
        void* params, double&)
{
    assert(dynamic_cast<const NKNetwork*>(&network));
    NKNetwork* net = (NKNetwork*)&network;

    std::vector<NKNode>& internal = net->getNetwork();

    const std::vector<std::vector<size_t>>* topology = (const std::vector<std::vector<size_t>>*) params;
    for (size_t iNode = from; iNode < to; ++iNode) {
        const BoolFunction function =
            BoolFunctionUtils::getRandomBoolVector(size_t(1)<<topology->at(iNode).size());
        const NKNode node = {topology->at(iNode), function};
        internal[iNode] = node;
    }
}


} // namepsace rbn
