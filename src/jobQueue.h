#ifndef JOB_QUEUE_INCLUDED
#define JOB_QUEUE_INCLUDED

#include <functional>
#include <queue>
#include <cassert>

#include "threadManager.h"

class JobQueue
{
public:
    typedef std::function<void()> Job;

    JobQueue(int threads);

    JobQueue(const JobQueue &) = delete;
    JobQueue &operator=(const JobQueue &) = delete;
    ~JobQueue();

    void start();
    void stop();
    void addJob(const Job &job);
    void waitDone();

private:
    int unlockedRemainingJobs() const;

private:
    /// The task that will run on the threads
    struct JobExecutor : Task
    {
        JobExecutor(JobQueue &queue);
        void run(int threadIndex, int threadCount) override;

        JobQueue &queue;
    };
    
    friend struct JobExecutor;

    std::queue<Job> jobs;        ///< All scheduled jobs that need to be executed
    ThreadManager threadManager; ///< The thread manager that will run our task
    JobExecutor executor;        ///< The task that will execute the jobs
    std::mutex workMtx;

    std::condition_variable haveWorkEvent;
    std::condition_variable doneWorkEvent;
    int activeWorkers = 0;
    bool isRunning = false; ///< True when running, set to false when JobQueue needs to stop immediately
};

#endif //JOB_QUEUE_INCLUDED
