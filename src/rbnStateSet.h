#ifndef RBN_STATE_SET_INCLUDED
#define RBN_STATE_SET_INCLUDED

#include <cstdlib>
#include <cstdint>
#include <initializer_list>

namespace rbn {

struct StateSet
{
    struct StateSetProxy
    {
    public:
        StateSetProxy(size_t& state, size_t index, size_t size);
        void operator=(bool funcVal);

        operator bool () const;

    private:
        size_t& state;
        size_t index;
        size_t size;
    };

public:
    explicit StateSet(size_t stateSz, size_t state = 0);
    StateSet(std::initializer_list<bool> l);

    StateSet(const StateSet&) = default;
    StateSet(StateSet&&) = default;

    StateSet& operator=(const StateSet&) = default;
    StateSet& operator=(StateSet&&) = default;

    StateSet& operator=(size_t state);

    ~StateSet() = default;

    operator size_t() const;

    StateSetProxy operator[] (size_t index);
    bool operator[] (size_t index) const;

    uint8_t size() const;
    void resize(size_t stateSz);

    static uint8_t maxNodesToEncode();

private:
    static constexpr uint8_t numOfEncodeBits();
    static constexpr size_t mask();
    static constexpr uint8_t distance();

    size_t state;
};

} // namespace rbn
#endif //RBN_STATE_SET_INCLUDED
