#ifndef RBN_STATE_SET_UTILS_INCLUDED
#define RBN_STATE_SET_UTILS_INCLUDED

#include <cstddef>
#include <random>

#include "rbnStateSet.h"

namespace rbn {

class XorGenerator64
{
public:
    XorGenerator64(size_t seed);
    size_t operator () ();
private:
    size_t state;
};

template <class RandEngine, class DistType>
class SSGenerator
{
public:
    static_assert(sizeof(RandEngine) == -1, "Generator not supproted");
    SSGenerator(size_t seed, size_t maxState);
    size_t operator() ();
private:
    RandEngine rng;
    DistType dist;
};


template <class RandEngine>
class SSGenerator<RandEngine, std::uniform_int_distribution<size_t>>
{
public:
    SSGenerator(size_t seed, size_t maxV);
    size_t operator() ();
private:
    RandEngine rng;
    std::uniform_int_distribution<size_t> dist;
};

template <>
class SSGenerator<XorGenerator64, std::uniform_int_distribution<size_t>>
{
public:
    SSGenerator(size_t seed, size_t maxVal);
    size_t operator() ();
private:
    XorGenerator64 rng;
    size_t maxVal;
};

template <class RandEngine>
class SSGenerator<RandEngine, std::uniform_real_distribution<double_t>>
{
public:
    SSGenerator(size_t seed, double_t maxV);
    double_t operator() ();
private:
    RandEngine rng;
    std::uniform_real_distribution<double_t> dist;
};


template <class RandEngine>
class SSGenerator<RandEngine, std::bernoulli_distribution>
{
public:
    SSGenerator(size_t seed, double prob = 0.5);
    bool operator() ();
private:
    RandEngine rng;
    std::bernoulli_distribution dist;
};

class StateSetUtils
{
public:
    struct Types
    {
        using UniformINTGenerator = SSGenerator<XorGenerator64, std::uniform_int_distribution<size_t>>;
        using UniformRealGenerator = SSGenerator<std::mt19937_64, std::uniform_real_distribution<double_t>>;
        using BernoulliGenerator = SSGenerator<std::mt19937_64, std::bernoulli_distribution>;
    };

    static Types::UniformINTGenerator getUnformIntGen(size_t maxVal);
    static Types::UniformRealGenerator getUnformRealGen(double_t maxVal);
    static Types::BernoulliGenerator getBernoulliGen(double prob = 0.5);
};

//------------------------------------------------------------------------------------------------
// Implementation
//------------------------------------------------------------------------------------------------

template <class RandEngine>
inline SSGenerator<RandEngine, std::uniform_int_distribution<size_t>>::
SSGenerator(size_t seed, size_t maxV)
    :rng(seed), dist(0, maxV)
{
}

template <class RandEngine>
inline size_t SSGenerator<RandEngine, std::uniform_int_distribution<size_t>>::operator() ()
{
    return dist(rng);
}

template <class RandEngine>
inline SSGenerator<RandEngine, std::uniform_real_distribution<double_t>>::
SSGenerator(size_t seed, double_t maxV)
    :rng(seed), dist(0, maxV)
{
}

template <class RandEngine>
inline double SSGenerator<RandEngine, std::uniform_real_distribution<double_t>>::operator() ()
{
    return dist(rng);
}

template <class RandEngine>
inline SSGenerator<RandEngine, std::bernoulli_distribution>::
SSGenerator(size_t seed, double prob)
    :rng(seed), dist(prob)
{}

template <class RandEngine>
inline bool SSGenerator<RandEngine, std::bernoulli_distribution>::operator() ()
{
    return dist(rng);
}

}// namespace rbn
#endif //#define RBN_STATE_SET_UTILS_INCLUDED
