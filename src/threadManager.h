#ifndef THREAD_MANAGER_INCLUDED
#define THREAD_MANAGER_INCLUDED

#include <vector>
#include <condition_variable>
#include <thread>
#include <mutex>

class ThreadManager;

struct Task {
    virtual void onBeforeRun(int threadCount) {(void)threadCount;}
    virtual void run(int threadIndex, int threadCount) = 0;

    void runOn(ThreadManager &tm);

    virtual ~Task() = default;
};

class ThreadManager
{
public:
    ThreadManager(const ThreadManager &) = delete;
    ThreadManager& operator=(const ThreadManager &) = delete;

    explicit ThreadManager(int threadCount);

    /// Start up all threads, must be called before @runThreads is called
    void start();

    /// Schedule the task to be run by the threads and return immediatelly
    /// This function could return before the threads have actually started running the task!
    void runThreadsNoWait(Task &task);

    /// Start a task on all threads and wait for all of them to finish
    /// @param task - the task to run on all threads
    void runThreads(Task &task);

    /// Start a task on single thread and return immediatelly
    /// @return false if there is no free thread
    bool addToQueueNoWait(Task &task);

    /// Blocking wait for all threads to exit, does not interrupt any running Task
    void stop();

    /// Get the number of worker threads
    size_t getThreadCount() const;

private:
    void threadBase(volatile int threadIndex);

    /// Check if all elements in @currentTask are nullptr
    /// @return true if at least one task is not nullptr, false otherwise
    bool unlockedAllTasksDone() const;

private:
    int count = -1; ///< The number of threads
    std::vector<std::thread> threads; ///< The thread handles

    bool running = false; ///< Flag indicating if threads should quit

    /// The current task for each thread, always must be the same element
    /// Used to track if thread has finished working
    std::vector<Task *> currentTask;
    std::mutex workMtx; ///< Mutex protecting @currentTask and @running

    /// The event used to signal workers when new task is available
    /// Also used by workers to signal when task is finished
    std::condition_variable workEvent;
};

#endif //THREAD_MANAGER_INCLUDED
