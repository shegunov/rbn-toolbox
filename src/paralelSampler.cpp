#include <cassert>
#include <iostream>
#include <string>
#include <sstream>
#include <atomic>

#include "syncWriter.h"
#include "threadManager.h"
#include "rbnTransitionMat.h"
#include "rbnNKModel.h"
#include "rbnNetworkAlg.h"

struct ParallelStatistics : Task
{
    ParallelStatistics (const char* fileName, size_t tableSize, size_t numTasks, size_t stepSize)
        : writer (fileName)
        , tableSize (tableSize)
        , numTasks (numTasks)
        , stepSize (stepSize)
    {
        assert(writer.isOpen());
    }

    void run(int threadIndex, int threadCount) override
    {
        double avgB;
        double avgC;
        std::pair<size_t, size_t> avgAttLenAndCnt;

        auto start = std::chrono::high_resolution_clock::now();
        size_t tasks = 0;

        // Need to do it as a factory
        rbn::TransitionMat table(tableSize);
        for (;;) {
            size_t samplesLeft = numTasks;
            size_t newSamples = 0;
            do {
                if (samplesLeft == 0) break;
                newSamples = samplesLeft - stepSize;
            } while (!std::atomic_compare_exchange_strong(&numTasks, &samplesLeft, newSamples));
            if (!samplesLeft) break;

            tasks += stepSize;
            for (size_t cnt = 0; cnt < stepSize; ++cnt) {
                size_t maxC = rbn::BNAlgorithms::sequential::fillWithRandom(table);
                avgC = rbn::BNAlgorithms::sequential::averageConnectivity(table);
                avgB = rbn::BNAlgorithms::sequential::averageBias(table);
                avgAttLenAndCnt = rbn::BNAlgorithms::sequential::averageAttractorLenAndCnt(table);

                assert(maxC >= avgC);
                (void)maxC;

                std::ostringstream str;
                str << std::fixed
                    << avgB << ", "
                    << avgC << ", "
                    << double(avgAttLenAndCnt.first) / avgAttLenAndCnt.second << ", "
                    << avgAttLenAndCnt.second << "\n";

                writer.writeString(str.str());
            }
        }
        auto duration = std::chrono::high_resolution_clock::now() - start;
        std::cout << threadIndex << " - "
                  << std::chrono::duration_cast<std::chrono::milliseconds>(duration).count()
                  << " num samples: " << tasks
                  << std::endl;
    }

    rbn::utils::SyncTextWriter writer;
    size_t tableSize;
    std::atomic<size_t> numTasks;
    size_t stepSize;
};


void runTestOnMultiThreads(
    size_t tableSize, size_t numTasks,
    int numThreads, const char* outputFile)
{
    ParallelStatistics parallelResult(outputFile, tableSize, numTasks, 5);

    ThreadManager threadManager(numThreads);
    threadManager.start();

    threadManager.runThreads(parallelResult);

    threadManager.stop();
}
