#include <cassert>

#include "rbnNetworkAlg.h"
#include "rbnNetwork.h"
#include "rbnNetworkAlgUtils.h"
#include "rbnNKModel.h"
#include "rbnTransitionMat.h"
#include "perfCounter.h"

namespace rbn {

#ifdef WITH_THREADS

typedef void (*processor) (const BooleanNetwork&,
                           size_t from, size_t to,
                           void* params,
                           double& result);

inline double accumulator(const BooleanNetwork& network, size_t numOfThreads, void* param,
                         processor op)
{
    size_t numOfNodes = network.nCols();
    size_t localSz = numOfNodes / numOfThreads;
    size_t left = numOfNodes % numOfThreads;

    std::vector<std::thread> threads(numOfThreads);
    std::vector<double> localResult(numOfThreads);

    for (size_t i = 0; i < numOfThreads; i++) {
        size_t from = (i * localSz) + (i < left ? i : left);
        size_t to = ((i + 1) * localSz) + ((i + 1) < left ? i + 1 : left);
        threads[i] = std::thread(op,
                                 std::ref(network),
                                 from,
                                 to,
                                 param,
                                 std::ref(localResult[i])
                                 );
    }

    for (size_t i = 0; i < numOfThreads; i++) {
        threads[i].join();
    }

    double result = 0;
    for (size_t i = 0; i < numOfThreads; i++) {
        result += localResult[i];
    }
    return result;
}
#endif

double BNAlgorithms::sequential::averageBias(const BooleanNetwork &network)
{
    countPerformance();

    double result = 0;
    NewtowrkUtils::averageBiasFromTo(network, 0, network.nCols(), nullptr, result);
    return  result;
}

double BNAlgorithms::sequential::averageConnectivity(const BooleanNetwork &network)
{
    countPerformance();

    double result = 0;
    NewtowrkUtils::averageConnecivityFromTo(network, 0, network.nCols(), nullptr, result);
    return  result;
}

std::pair<size_t, size_t> BNAlgorithms::sequential::averageAttractorLenAndCnt(const BooleanNetwork &network)
{
    countPerformance();

    size_t sum = 0;
    size_t cnt = 0;
    size_t nNodes = network.nCols();
    size_t result = 0;
    std::pair<size_t, size_t> fromTo;
    std::vector<char> visited(network.nRows(), false);

    for (size_t i = 0; i < nNodes; i++) {
        if (!visited[i]) {
            result = NewtowrkUtils::findAttractor(network, i, fromTo, visited);
            if (result) {
                sum += result;
                cnt++;
            }
        }
    }

    return  std::pair<size_t, size_t> (sum, cnt);
}

size_t BNAlgorithms::sequential::fillRandom(BooleanNetwork& network, size_t maxConn)
{
    countPerformance();

    using Gen = StateSetUtils::Types::UniformINTGenerator;
    Gen gen = StateSetUtils::getUnformIntGen(network.nCols() - 1);

    std::vector<std::vector<size_t>> topology(network.nCols(), std::vector<size_t>(maxConn, 0));
    for(auto& node : topology) {
        for(auto& ancestors : node) {
            ancestors = gen();
        }
    }
    NKNetwork nkNetwork;
    nkNetwork.setTopology(topology);

    TransitionMat* trMat = dynamic_cast<TransitionMat*>(&network);
    if (trMat != nullptr) {
        *trMat = nkNetwork;
    }

    return maxConn;
}

size_t BNAlgorithms::sequential::fillWithRandom(BooleanNetwork& network)
{
    countPerformance();

    StateSetUtils::Types::UniformINTGenerator gen = StateSetUtils::getUnformIntGen(network.nCols() -1);
    return fillRandom(network, gen());
}

#ifdef WITH_THREADS

double BNAlgorithms::parallel::averageBias(const BooleanNetwork &network, size_t numOfThreads)
{
    countPerformance();

    return accumulator(network, numOfThreads, nullptr, NewtowrkUtils::averageBiasFromTo);
}

double BNAlgorithms::parallel::averageConnectivity(const BooleanNetwork &network, size_t numOfThreads)
{
    countPerformance();

    return accumulator(network, numOfThreads, nullptr, NewtowrkUtils::averageConnecivityFromTo);
}

size_t BNAlgorithms::parallel::fillWithRandom(BooleanNetwork& network, size_t numOfThreads)
{
    countPerformance();

    StateSetUtils::Types::UniformINTGenerator gen = StateSetUtils::getUnformIntGen(network.nCols() -1);
    return fillRandom(network, gen(), numOfThreads);
}

size_t BNAlgorithms::parallel::fillRandom(BooleanNetwork& network, size_t maxConn, size_t numOfThreads)
{
    countPerformance();

    using Gen = StateSetUtils::Types::UniformINTGenerator;
    Gen gen = StateSetUtils::getUnformIntGen(network.nCols() - 1);

    std::vector<std::vector<size_t>> topology(network.nCols(), std::vector<size_t>(maxConn, 0));
    for(auto& node : topology) {
        for(auto& ancestors : node) {
            ancestors = gen();
        }
    }

    NKNetwork nkNetwork;

    std::vector<NKNode>& net = nkNetwork.getNetwork();
    net.clear();
    net.resize(topology.size());
    assert(net.size() == network.nCols());

    accumulator(nkNetwork, numOfThreads, &topology, NewtowrkUtils::randomFillerFromTo);

    TransitionMat* trMat = dynamic_cast<TransitionMat*>(&network);
    if (trMat != nullptr) {
        *trMat = nkNetwork;
    }

    return maxConn;
}

#endif

}
